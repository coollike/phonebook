import java.io.*;
import java.rmi.AccessException;

public class PhoneBookCreator {
		private PhoneNumberValidator phoneNumberValidator;
		private RecordParser recordParser;
		
		private FileReader reader;
		private FileWriter writer;
		
		private String stringTerminatorValue;
		
		 public PhoneBookCreator(PhoneNumberValidator phoneNumberValidator,
                 RecordParser recordParser,
                 FileReader reader,
                 FileWriter writer,
                 String stringTerminatorValue) {
			 if(phoneNumberValidator == null) {
				 	String exceptionMsg = ValidationMessage.nullParameter;
					System.out.println(exceptionMsg);
			 }
			 else if(reader ==null) {
				 String exceptionMsg = ValidationMessage.nullParameter;
					System.out.println(exceptionMsg);
			 }
			 
			 this.reader=reader;
			 this.writer=writer;
			 this.stringTerminatorValue = stringTerminatorValue;
		 }
		 private void FillCreatedPhoneBook(PhoneBook createdPhoneBook) {
			 String readString=this.reader.ReadLine();
			 while(readString!=this.stringTerminatorValue) {
				 Record record=null;
				 
				 record = this.recordParser.Parse(readString);
				 
				 ActionResult result = createdPhoneBook.AddRecord(record.getName(), record.getPhoneNumber());
				 
				 if (!result.isSuccessful) {
					 this.writer.Write(result.getValidationMessage());
				 }
				 
				 readString = this.reader.ReadLine();
				 
			 }
		 }
		 
		 public PhoneBook CreatePhoneBook() {
			 PhoneBook createdPhoneBook = new PhoneBook(this.phoneNumberValidator);
			//using

			 this.FillCreatedPhoneBook(createdPhoneBook);
			 return createdPhoneBook;
			 
		 }
}
