
public class Record {
	private String Name;
	private String PhoneNumber;

	public Record(String name, String phoneNumber) {
		this.setName(name);
		this.setPhoneNumber(phoneNumber);
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}
}
