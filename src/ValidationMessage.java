
public class ValidationMessage {
	public static String nameExists = "The name you are trying to enter exists.";
	public static String nameDoesNotExist = "The name you are trying to enter does not exist in the phonebook.";
	public static String invalidPhoneNumber = "The entered phone number is invalid.";
	
	public static String invalidRecord = "Not enough arguments are passed to create a new record";
	
	public static String invalidFileName = "File name cannot be empty.";
	
	public static String nullParameter = "This cannot be null";
	
}
