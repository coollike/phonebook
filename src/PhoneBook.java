import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
public class PhoneBook {
 
	private PhoneNumberValidator phoneNumberValidator;
	private Map<String, String> allNumbersByName;
	
	public PhoneBook (PhoneNumberValidator phoneNumberValidator) {
		if (phoneNumberValidator == null) {
			String exceptionMsg = ValidationMessage.nullParameter;
			System.out.println(exceptionMsg);
		}
		
		this.phoneNumberValidator= phoneNumberValidator;
	}
	
	public ActionResult AddRecord(String name, String phoneNumber) {
		Boolean recordIsAdded=false;
		String validationMessage="";
		if (this.allNumbersByName.containsKey(name)) {
			validationMessage=ValidationMessage.nameExists;
		}
		else if(!this.phoneNumberValidator.NumberIsValid(phoneNumber)){
		}
		else {
			recordIsAdded=true;
			
			this.allNumbersByName.put(name,phoneNumber);
		}
	
		ActionResult actionResult = new ActionResult(recordIsAdded, validationMessage);
        
		return actionResult;
	}
	
	public String RemoveRecord(String name) {
		return this.allNumbersByName.remove(name);
	}
	
	public Map<String, String> GetPhoneNumber(String name) {
		if (!this.allNumbersByName.containsKey(name)) {
			String exceptionMsg = ValidationMessage.nameDoesNotExist;
			System.out.println(exceptionMsg);
		}
		 return this.allNumbersByName;
	}
	
	public String getAllRecords() {
		StringBuilder allRecords = new StringBuilder();
		
		for (KeyValuePair<string, string> nameAndPhone in this.allNumbersByName){
			String format = FormattingMessage.SingleRecordFormat;
            String name = nameAndPhone.Key;
            String phone = nameAndPhone.Value;

            String formattedRecord = String.format(format, name, phone);
            allRecords.append(formattedRecord);
		}
		   return allRecords.toString().trim();
	}



}
