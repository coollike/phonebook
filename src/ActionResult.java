
public class ActionResult {
	
	public Boolean isSuccessful;
	private String validationMessage;

	public ActionResult(Boolean isSuccessfull, String validationMessage) {
		this.isSuccessful = isSuccessfull;
		this.setValidationMessage(validationMessage);
	}

	public String getValidationMessage() {
		return validationMessage;
	}

	public void setValidationMessage(String validationMessage) {
		this.validationMessage = validationMessage;
	}
	
}
