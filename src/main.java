import java.util.regex.*; 


public class main {

	public static void main(String[] args) {
		RegexChecker.regexChecker("^(?:\\+359|00359|0)8[7-9][2-9][0-9]{6}$", validPhoneNumber);
		
		//String regexPattern = "^(?:\\+359|00359|0)8[7-9][2-9][0-9]{6}$";
		
		PhoneNumberValidator phoneNumberValidator = new PhoneNumberValidator(validPhoneNumber);
		
		RecordParser recordParser = new RecordParser (',',' ');
		
		String filePath = "D:\\Stuff\\Eclipse Workspace\\Phonebook\\file.txt";
		FileReader reader = new FileReader(filePath);
		FileWriter writer = new FileWriter();
		
		
		String stringTerminationValue = Constant.StringTerminatorValue;
		
		 PhoneBookCreator phoneBookCreator = new PhoneBookCreator(phoneNumberValidator,
                 recordParser,
                 reader,
                 writer,
                 stringTerminationValue);
		 PhoneBook createdPhoneBook = phoneBookCreator.CreatePhoneBook();
	}

}
